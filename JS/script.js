const tabsList = document.querySelectorAll('.tabs-title');
const tabsContentList = document.querySelectorAll('.tabs-content li');

//Додала data-атрібути, вони вказані у літературі до дом.роботи, вирішила потренуватись з ними.
function setDataAttributes() {
    tabsList.forEach(function (element) {
        element.setAttribute('data-title', `${element.innerHTML}`);
    })

    let counter = 0;
    tabsContentList.forEach(function (element) {
        element.setAttribute('data-about', `${tabsList[counter].innerHTML}`);
        counter++;
    })
};

setDataAttributes();

// Робимо на початковому етапі всі елементи invisible ,а з класом active - visible,можна було зробити в html/css вирішила потренуватись в js.
tabsContentList.forEach(function (element) {
    element.classList.add('invisible');
});
tabsList.forEach(function (element) {
    if(element.classList.contains('active')){
       let text =  document.querySelector(`li[data-about ="${element.dataset.title}"]`);
       text.classList.add('visible');
    }
})
//

function switchTabs() {
    tabsList.forEach(function (elementTab) {
        elementTab.addEventListener('click', function (event) {
            tabsList.forEach(function (elementTab){
                elementTab.classList.remove('active');
            })
                elementTab.classList.add('active');
            
            tabsContentList.forEach(function (element) {
                element.classList.remove('visible');
                element.classList.add('invisible');

                if (element.dataset.about === event.target.dataset.title) {
                    element.classList.remove('invisible');
                    element.classList.add('visible');
                }
            });
        })
    });
}

switchTabs();